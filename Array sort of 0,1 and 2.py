from itertools import count
import math
 
def func(arr, n):
 
    # counting no of 0,1,2 in array
    count0 = 0
    count1 = 0
    count2 = 0
    for i in range(0,n):
        if (arr[i] == 0):
            count0=count0+1
        if (arr[i] == 1):
            count1=count1+1
        if (arr[i] == 2):
            count2=count2+1
     
    print (count0, ' time no 0 present in array', )
    print ( count1 ,  ' time no 1 present in array')
    print ( count2 ,  ' time no 2 present in array')
 
    # putting 0 in stating
    for i in range(0,count0):
        arr[i] = 0
     
    # Putting the 1 in the array after the 0
    for i in range( count0, (count0 + count1)) :
        arr[i] = 1
     
    # Putting the 2 in the array after the 1
    for i in range((count0 + count1),n) :
        arr[i] = 2    
    return
 
 
def printArray( arr,  n):
    # print (count0)
    # print (count1)
    # print (count2)
    for i in range(0,n):
        print( arr[i] , end=" ")
    print()
 
 

arr = [ 1,0,2,1,0,2,0,0,1,1,2,1]
n = len(arr)
func(arr, n)
printArray(arr, n)
